#! /bin/bash

# This script opens the ANTLRWorks editor, and the WCPS grammar in it, for editing. 
java -Xms128m -Xmx300m -jar ../lib/antlrworks-1.2.3.jar ../src/java/grammar/wcps.g

